# Python 패키징 개요
범용 프로그래밍 언어인 Python은 다양한 방법으로 사용할 수 있도록 설계되었다. 같은 핵심 기술을 사용하여 웹 사이트, 산업용 로봇, 또는 친구들이 즐길 수 있는 게임 등을 만들 수 있다.

Python의 유연성으로 인하여 모든 Python 프로젝트에서 첫 번째 단계로 프로젝트의 대상자와 프로젝트가 실행될 환경에 대해 생각해야 한다. 코드를 작성하기 전에 패키징에 대해 생각하여야 하는 것이 이상하게 보일 수 있지만, 이 과정은 향후 닥칠 어려움을 피하는 데 매우 효과적이다.

이 개요에서는 Python의 수많은 패키징 옵션에서 선택을 위한 근거를 제공한다. 다음 프로젝트에서 가장 적합한 테크놀로지를 선택하기 위해 알아 보도록 한다.

**목차**

- [전개(deployment)에 대한 고려](#thinking-about-deployemnt)
- [Python 라이브러리와 도구 패키징](#packaging-python-libraries-and-tools)
- [Python 애플리케이션 패키징](#packaging-python-applications)
- [기타](#what-about)
- [요약](#wrap-up)


## 전개(deployment)에 대한 고려 <a id="thinking-about-deployemnt"></a>
패키지는 인스톨(또는 *전개(deployed)*)해야 하기 때문에, 패키징하기 전에, 아래의 전개에 관한 질문에 대한 답변이 필요하다.

- 소프트웨어의 사용자는 누구인가? 소프트웨어 개발을 하는 다른 개발자, 데이터 센터의 운영자 또는 소프트웨어에 익숙하지 못한 사용자 그룹이 소프트웨어를 설치하도록 되었는가?

- 소프트웨어는 서버, 데스크톱, 모바일 클라이언트(스마트 폰, 태블릿 등)에서 실행되도록 설계되었는가? 또는 전용 기기에 내장되어 있는가?

- 소프트웨어는 개별적으로 설치되는가, 아니면 대규모 배치로 설치되는가?

패키징은 타깃 환경과 도입 경험에 관한 것이다. 위의 질문에는 많은 답변이 있으며 각각의 상황 조합에따라 고유한 해결책이 있다. 이 정보를 바탕으로 프로젝트에 가장 적합한 패키징 기술을 소개한다.

## Python 라이브러리와 도구 패키징 <a id="packaging-python-libraries-and-tools"></a>
`PyPI`, `setup.py` 및 `wheel` 들에 대해 들어 보았을 것이다. 이들은 Python의 생태계가 Python 코드를 개발자에게 배포하기 위해 제공하는 툴의 일부에 불과하다. [프로젝트 패키징과 배포](https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/)에서 이 툴들을 찾아 볼 수 있다.

패키징에 대한 다음 접근방식은 개발 환경에서 개발자들이 사용하는 라이브러리와 도구를 대상으로 한다. [Python 애플리케이션 패키징](https://packaging.python.org/en/latest/overview/#packaging-applications)은 일반 사용자 또는 프로덕션 환경을 위한 Python 패키징 방법을 설명하고 있다.

### Python 모듈 <a id="python-module"></a>
표준 라이브러리만 사용하는 Python 파일은 재배포와 재사용할 수 있다. 또한 Python 코드가 Python 버전에 맞게 작성되었으며 표준 라이브러리만 사용하는지 확인해야 한다.

이는 (이메일, StackOverflow, GitHub gists 처럼) 호환성이 있는 Python 버전을 사용하는 사용자들 간에 간단한 스크립트와 스니펫을 공유하는 데 매우 적합하다. 심지어 [bottle.py](https://bottlepy.org/docs/dev/)이나 [boltons](https://boltons.readthedocs.io/en/latest/architecture.html)과 같이 이들을 옵션으로 제공하는 Python 라이브러리도 있다.

그러나 이 패턴은 Python 프로젝트에서 코드가 여러 파일로 구성되거나 추가적으로 라이브러리가 필요하거나 특정 버전의 Python이 필요한 경우에는 확장되지 않는다. 따라서 아래 옵션을 사용할 수 있다.

### Python 소스 배포 <a id="python-source-distributions"></a>
코드가 여러 Python 파일로 구성되어 있는 경우 보통 디렉토리 구조를 따른다. Python 파일을 저장하고 있는 디렉토리를 [Import Package](https://packaging.python.org/en/latest/glossary/#term-Import-Package)로 구성할 수 있다.

패키지는 여러 파일로 구성되므로 배포하기가 더 어렵다. 대부분의 프로토콜은 한 번에 하나의 파일만 전송할 수 있다 (마지막으로 링크를 클릭하여 여러 파일을 다운로드한 것이 언제인가?). 불완전한 전송은 쉽고, 목적지에서 수신한 코드의 무결성을 보장하는 것은 어렵다.

순수 Python 코드만 있고 배포 환경에서 Python 버전을 지원하는 것을 알고 있다면 Python의 네이티브 패키징 도구를 사용하여 *source* [Distribution Package](https://packaging.python.org/en/latest/glossary/#term-Distribution-Package)(sdist)를 만들 수 있다.

Python의 *sdist*는 하나 이상의 패키지 또는 모듈을 포함하는 압축 아카이브(`.tar.gz` 파일)이다. 만약 코드가 순수 Python이고 다른 Python 패키지만 사용한다면, [여기](https://docs.python.org/3/distutils/sourcedist.html)에서 더 많은 정보를 얻을 수 있다.

Python 코드 또는 Python 패키지가 아닌 졍우 ([lxml](https://pypi.org/project/lxml/)의 경우 [libxml2](https://en.wikipedia.org/wiki/Libxml2) 또는 [numpy](https://pypi.org/project/numpy)의 경우 BLAS 라이브러리)에 의존하는 경우 다음 섹션에서 설명하는 형식을 사용해야 한다. 이 형식도 순수 Python 라이브러리에 비교하여 장점을 갖고 있다.

> **Note**: Python과 PyPI는 동일한 패키지의 다양한 구현을 제공할 수 있도록 여러 배포 버전을 지원한다. 예를 들어 유지 관리는 되지 않지만 중요한 [PIL 배포](https://pypi.org/project/PIL/)는 PIL 패키지를 제공하며, [Pillow](https://pypi.org/project/Pillow/)는 PIL의 fork를 통하여 활발히 유지 보수되고 있다.
> 이 Python 패키징의 초강력 기능 덕분에 프로젝트의 `install_requires` 또는 `requirements.txt`을 변경하는 것만으로 Pillow가 PIL를 대신할 수 있다.

### Python 바이너리 배포 <a id="python-binary-distributions"></a>
Python의 실질적인 힘은 소프트웨어 생태계, 특히 C, C++, Fortran, Rust 및 기타 언어로 작성된 라이브러리와의 통합 능력에서 오는 것이다.

모든 개발자가 컴파일된 언어로 작성된 컴포넌트를 빌드하는 데 적합한 툴이나 경험을 가지고 있는 것은 아니기 때문에 Python은 컴파일된 아티팩트와 함께 라이브러리를 제공하도록 설계된 패키지 형식인 [Wheel](https://packaging.python.org/en/latest/glossary/#term-Wheel)을 만들었다. 사실 Python의 패키지 설치 프로그램인 `pip`은 설치 속도가 빠르기 때문에 wheel을 선호한다. 따라서 순수 Python 패키지도 wheel에서 더 잘 작동한다.

이항 분포는 일치하는 소스 분포와 함께 제공되는 경우에 가장 적합합니다(?). 모든 운영 체제에서 코드 wheel이 업로드되지 않더라도 sdist를 업로드하면 다른 플랫폼의 사용자가 직접 코드를 빌드할 수 있다. 수신자는 둘 중 하나만 필요로 하는 매우 구체적인 사용 사례에 대해 아티팩트를 생성하는 경우를 제외하고 기본적으로 sdist 아카이브와 휠 아카이브를 함께 발표한다.

Python과 PyPI를 사용하면 휠과 sdist를 모두 쉽게 업로드할 수 있다. [Packaging Python Projects](https://packaging.python.org/en/latest/tutorials/packaging-projects/) 튜토리얼을 따라 한다.

![pic-pde-01](images/py_pkg_tools_and_libs.png)
Python의 권장 내장 라이브러리 및 도구 패키징 기술. [패키징 그라데이션(2017)에서 발췌](https://www.youtube.com/watch?v=iLVNWfPWAC8).

## Python 애플리케이션 패키징 <a id="packaging-python-applications"></a>
지금까지 Python의 네이티브 배포 툴에 대해서만 설명했다. 이 방법은 Python이 설치되어 있는 환경과 Python 패키지를 설치하는 방법을 알고 있는 사용자만을 대상으로 하는 것이 옳습니다.  

일반적으로 다양한 운영 체제, 구성 및 사용자을 대상으로 하고 있기 때문에 이러한 가정은 개발자를 대상으로 하는 경우에만 해당된다.

Python의 네이티브 패키징은 주로 라이브러리라고 불리는 재사용 가능한 코드를 개발자 간에 배포하기 위해 구축되었다. [setuptools entry_points](https://setuptools.pypa.io/en/latest/userguide/entry_point.html)와 같은 기술을 사용하여 Python 라이브러리 패키징 위에 도구 또는 개발자를 위한 기본 애플리케이션을 올릴 수 있다.

라이브러리는 완전한 애플리케이션이 아닌 구성 요소입니다. 애플리케이션 배포에는 전혀 새로운 테크놀로지를 도입하였다.

다음 섹션에서는 대상 환경에 대한 의존성에 따라 이러한 애플리케이션 패키징 옵션을 정리함으로써 프로젝트에 적합한 옵션을 선택할 수 있다.

### 프레임워크에 대한 의존 <a id="depending-on-a-framework"></a>
웹 사이트 백엔드와 기타 네트워크 서비스와 같은 유형의 Python 애플리케이션은 개발 및 패키징을 가능하게 하는 프레임워크를 가지고 있을 정도로 충분히 일반적이다. 동적 웹 프런트엔드 및 모바일 클라이언트와 같은 다른 유형의 애플리케이션은 프레임워크가 편리성 이상의 것을 목표로 할 정도로 복잡하다.

이러한 모든 경우 프레임워크의 패키징과 전개 사례에서 bottom-up으로 작업하는 것이 타당하다. 일부 프레임워크에서는 가이드의 부록에 전개 시스템의 기술을 포괄적으로 기술을 설명하고 있다. 이 경우 가장 쉽고 신뢰성 높은 프로덕션 환경을 위해 프레임워크의 패키징 가이드를 따르는 것이 바람직하다.

이러한 플랫폼과 프레임워크의 작동이 궁금하면 아래 섹션을 참고하도록 한다.

#### 서비스 플랫폼 <a id="service-platforms"></a>
`Heroku`, `Google App Engine` 등에서 "PaaS(Platform-as-a-Service)"를 개발하려면 각각의 패키징 가이드를 따라야 한다.

- [Heroku](https://devcenter.heroku.com/articles/getting-started-with-python)
- [Google App Engine](https://cloud.google.com/appengine/docs/python/)
- [PythonAnywhere](https://www.pythonanywhere.com/)
- [OpenShift](https://blog.openshift.com/getting-started-python/)
- [Zappa](https://github.com/zappa/Zappa)와 같은 “Serverless” 프레임워크

이러한 모든 설정에서는, 그 패턴을 따르면 플랫폼이 패키징과 전개을 담당한다. 대부분의 소프트웨어는 이러한 템플릿 중 하나에도 적합하지 않을 수 있으므로 다음 옵션들이 존재한다.

유저의 퍼스널 컴퓨터나 그 외의 방법 등으로 가용한 머신에 전개할 소프트웨어를 개발하고 있는 경우 아래를 참조한다.

#### 웹 브라우저와 모바일 애플리케이션 <a id="web-browsers-and-mobile-applications"></a>
Python의 지속적인 발전은 Python을 새로운 공간으로 이끌고 있다. 최근 모바일 앱이나 웹 애플리케이션 프런트엔드를 Python으로 작성할 수 있다. 언어에는 익숙할 수 있지만 패키징 및 배포 방법은 완전히 다른 것다.

이러한 새로운 영역으로 집입할 예정이라면, 다음 프레임워크를 확인하여 패키징 가이드를 참조하여야 한다.

- [Kivy](https://kivy.org/)
- [Beeware](https://pybee.org/)
- [Brython](https://brython.info/)
- [Flexx](https://flexx.readthedocs.io/en/latest/)

프레임워크 또는 플랫폼 사용에 관심이 없거나 위의 프레임워크에서 사용되는 기술 및 기술에 대해 궁금한 경우 아래를 계속 읽어 보도록 한다.

### 설치된 Python에서 <a id="depending-on-a- pre-installed-python"></a>
임의의 컴퓨터를 선택하면 상황에 따라 다르겠지만 Python이 이미 설치되어 있을 가능성이 매우 높다. 수년 동안 대부분의 Linux 및 Mac 운영 체제에 기본적으로 포함되어 있는 Python은 개발자와 데이터 과학자의 데이터 센터나 개인 컴퓨터에 상당히 의존할 수 밖에 없다.

이 모델을 지원하는 기술은 다음과 같다.

- [PEX](https://github.com/pantsbuild/pex#user-content-pex) (Python EXecutable)
- [zipapp](https://docs.python.org/3/library/zipapp.html) (종속성 관리를 지원 않음, Python 3.5이상 필요)
- [shiv](https://github.com/linkedin/shiv#user-content-shiv) (Python 3 필요)

> **Note**: 이 모든 접근법은 미리 설치된 Python 환경에 가장 많이 의존한다. 물론, 이것은 수 메가바이트 또는 킬로바이트의 작은 패키지도 만든다.
> 일반적으로 타깃 시스템에 대한 의존도를 낮추려면 패키지의 크기가 커지기 때문에 이들의 해결책으로 출력 크기를 늘리는 것으로 대략 정리할 수 있다.

### 단일 소프트웨어 배포 생태계로 <a id="depending-on-a-seperate-software-distribution-ecosystem"></a>
오랫동안 Mac과 Windows를 포함한 많은 운영 체제에는 패키지 관리 기능이 내장되어 있지 않았다. 이러한 OS는 최근에야 소위 "앱 스토어"를 열었지만, 이러한 OS조차도 소비자 애플리케이션에 초점을 맞추고 개발자들에게는 거의 제공하지 않았다.

개발자들은 오랫동안 해결책을 찾았고, 이 투쟁에서 Homebrew와 같은 자체 패키지 관리 솔루션을 만들었다. Python 개발자들에게 가장 적절한 대안은 Anaconda라고 불리는 패키지 생태계이다. Anaconda는 Python을 중심으로 구축되었으며 학술, 분석 및 기타 데이터 지향 환경에서 점점 더 보편화되고 있으며, 서버 지향 환경에서도 마찬가지이다.

Anaconda 생태계를 위한 구축과 발표 절차는 아래와 같다.

- [conda를 이용한 라이브러리와 애플리케이션 구축](https://conda.io/projects/conda-build/en/latest/user-guide/tutorials/index.html)
- [네이티브 Python 패키지를 Anaconda로 변환](https://conda.io/projects/conda-build/en/latest/user-guide/tutorials/build-pkgs-skeleton.html)

유사한 모델에는 대체 Python 배포판을 설치하는 것이 포함되지만 임의 운영 체제 수준 패키지는 지원하지 않는다.

- [activeState ActivePython](https://www.activestate.com/activepython)
- [WinPython](http://winpython.github.io/)

### 자신의 Python Executable로 <a id="bring-your-own-python-executable"></a>
우리가 알고 있는 컴퓨팅은 프로그램을 실행할 수 있는 능력으로 정의한다. 모든 운영체제는 기본적으로 실행할 수 있는 하나 이상의 프로그램 형식을 지원하고 있다.

Python 프로그램을 이러한 형식 중 하나로 만드는 많은 방법과 기술을 지원하며, 대부분이 Python 인터프리터와 기타 종속성을 하나의 실행 파일에 내장하여 실행파일을 만드는 것과 관련이 있다.

*freeaing*이라고 불리는 이 접근방식은 광범위한 호환성과 심리스한 사용자 경험을 제공하지만 많은 경우 다양한 기술과 많은 노력이 필요하다.

다음과 같은 Python `freezer`들이 있다.

- [pyInstaller](https://pyinstaller.readthedocs.io/en/stable/) - Cross-platform
- [cx_Freeze](https://marcelotduarte.github.io/cx_Freeze) - Cross-platform
- [constructor](https://github.com/conda/constructor) - For command-line installers
- [py2exe](http://www.py2exe.org/) - Windows only
- [py2app](https://py2app.readthedocs.io/en/latest/) - Mac only
- [osnap](https://github.com/jamesabel/osnap) - Windows and Mac
- [pynsist](https://pypi.org/project/pynsist/) - Windows only

위에서 열거한 freezer 대부분은 단일 사용자를 위한 전개를 지원한다. 멀티 컴포넌트 서버 애플리케이션의 경우는, [Chef Omnibus](https://github.com/chef/omnibus#user-content--omnibus) 를 참조하도록 한다.

### 자신의 사용자공간(userspace)으로 <a id="bring-your-own-userspace"></a>
[operating system 레벨의 가상화](https://en.wikipedia.org/wiki/Operating-system-level_virtualization) 또는 *containerization*이라고 불리는 비교적 최신 방법를 사용해 경량 이미지로 패키지화된 애플리케이션을 실행하도록 설정할 수 있는 Linux, Mac OS, Windows 등을 포함하는 OS가 증가하고 있다.

이러한 기술은 Python이나 Python 패키지가 아닌 전체 OS 파일 시스템을 패키징하기 때문에 대부분 Python에 의존하지 않는다.

Linux 서버에서 도입이 가장 광범위하게 이루어지고 있다. Linux는 기술이 시작된 곳이며 가장 잘 작동하는 플랫폼이다.

- [AppImage](https://appimage.org/)
- [Docker](https://www.fullstackpython.com/docker.html)
- [Flatpak](https://flatpak.org/)
- [Snapcraft](https://snapcraft.io/)


### 자신의 커널로 <a id="bring-your-own-kernel"></a>
대부분의 운영체제는 어떤 형태로 든 기존 가상화를 지원하며, 애플리케이션에 자체 운영체제를 포함하는 이미지로 패키지화한다. 이러한 가상 머신(VM)의 실행은, 데이터 센터 환경에 널리 퍼져 사용되는  어프로치이다.

이 기술 대부분 데이터센터에서 대규모로 도입되고 있지만, 이 패키징을 통해 일부 복잡한 어플리케이션은 이점을 얻을 수 있다. Python에 구애받지 않는 기술은 다음과 같다.

- [Vagrant](https://www.vagrantup.com/)
- [VHD](https://en.wikipedia.org/wiki/VHD_(file_format)), [AMI](https://en.wikipedia.org/wiki/Amazon_Machine_Image)과 [다른 방법](https://docs.openstack.org/glance/latest/user/formats.html)
- [OpenStack](https://www.redhat.com/en/topics/openstack) - 광범위하게 VM을 지원하는 Python의 클라우드 관리 시스템

### 자신의 하드웨어로 <a id="bring-your-own-hardware"></a>
소프트웨어를 출하하는 가장 포괄적인 방법은 일부 하드웨어에 설치하여 출하하는 것이다. 이렇게 하면 소프트웨어 사용자에게 필요한 것은 전력뿐일 것이다.

위에서 설명한 가상 머신은 주로 기술에 정통한 사용자를 위한 것이지만, 하드웨어 어플라이언스는 최첨단 데이터 센터에서 가장 어린 아이까지 모든 사용자가 사용할 수 있습니다.

코드를 [Adafruit](https://github.com/adafruit/circuitpython), [MicroPython](https://micropython.org/) 또는 Python을 실행하는 보다 강력한 하드웨어에 설치한 후 데이터 센터 또는 사용자의 집으로 보내 플러그 앤 플레이를 하는 것으로 끝이다.

![pic-pde-02](images/py_pkg_applications.png)
Python 어플리케이션 패키징에 사용되는 모든 테크놀로지의 단순화.

## 기타 <a id="what-about"></a>
위의 섹션들에서 요약할 수 있는 것은 한정되어 있다.또, 보다 현저한 차이가 얼만큼인지 궁금할 수도 있다.

### 운영체제 패키지 <a id="operation-system-package"></a>
위 [단일 소프트웨어 배포 생태계로](#depending-on-a-seperate-software-distribution-ecosystem)에서 설명한 바와 같이 일부 운영 체제에는 자체 패키지 매니저가 있습니다. 대상 운영체제를 확실히 안다면 [deb](https://en.wikipedia.org/wiki/Deb_(file_format))(Debian, Ubuntu 등) 또는 [RPM](https://en.wikipedia.org/wiki/RPM_Package_Manager)(Red Hat, Fedora 등) 등의 포맷에 직접 맞추어 설치와 전개할 수 있다. [FPM](https://fpm.readthedocs.io/en/latest/cli-reference.html#virtualenv)을 사용하여 동일한 소스에서 deb와 RPM을 모두 생성할 수도 있다.

대부분의 전개 파이프라인에서 OS 패키지 매니저는 퍼즐의 일부에 불과하다.

### virtualenv <a id="virtualenv"></a>
[Virtualenvs](https://docs.python-guide.org/dev/virtualenvs/)는 여러 세대 Python 개발자들에게 없어서는 안 될 도구였지만, 더 높은 수준의 도구에 둘러싸여 있기 때문에 서서히 사라지고 있다. 특히 패키징에서는 virtualenvs는 [dh-virtualenv 도구]()와 [osnap]()에서 프리미티브로 사용되며 둘 다 virtualenvs를 자기 함유 방식으로 싼다(wrap).

프로덕션 환경에서 전개는 개발 환경에서와 같이 인터넷에서 가상 환경으로의 `python -m pip install` 실행에 의존하지 마십시오. 훨씬 더 나은 해결책이 앞선 개요에 가득하다.

### 보안 <a id="security"></a>
내부로 더 내려갈수록 패키지의 컴포넌트를 업데이트하기가 더 어려워진다. 모든 것이 더 긴밀하게 엮여 있다.

예를 들어 커널 보안 문제가 발생하여 컨테이너를 전개한 경우, 애플리케이션 새 빌드를 요구하지 않고 대신 호스트 시스템의 커널을 업데이트할 수 있다. VM 이미지를 전개하려면 새로운 빌드가 필요하다. 이 다이내믹스를 통해 하나의 옵션이 더 안전해질지 여부는 여전히 오래된 논쟁거리 이다. 이는 [정적 링킹와 동적 링킹](https://www.google.com/search?channel=fs&q=static+vs+dynamic+linking) 간 아직 해결되지 않은 오래된 문제이다.

## 요약 <a id="#wrap-up"></a>
Python에서 패키징은 다소 험난한 것으로 알려져 있다. 대부분 Python의 융통성에서 기인한다고 생각된다. 각 패키징 솔루션 간의 자연스러운 경계를 이해하면 Python 프로그래머는 가장 균형잡히고 유연한 언어 중 하나인 Python을 사용하기 위해 지불하여야 하는 작은 노력이라는 것을 깨닫게 될 것이다.
